package ru.ava.patient;

public class Patient {
    private String sername;
    private int birth;
    private int numberkart;
    private boolean dispanse;

    Patient(String sername, int birth, int numberkart, boolean dispanse) {
        this.sername = sername;
        this.birth = birth;
        this.numberkart = numberkart;
        this.dispanse = dispanse;
    }

    Patient() {
        this("не указана", 0, 0, false);
    }

    public String getSername() {
        return sername;
    }

    public int getBirth() {
        return birth;
    }

    public int getNumberkart() {
        return numberkart;
    }

    public boolean isDispanse() {
        return dispanse;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "sername='" + sername + '\'' +
                ", birth=" + birth +
                ", numberkart=" + numberkart +
                ", dispanse=" + dispanse +
                '}';
    }


}
