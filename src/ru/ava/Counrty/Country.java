package ru.ava.Counrty;

class CountryBox {
    private String name;
    private String capital;
    private double territory;
    private int naselenie;

    CountryBox(String name, String capital, double territory, int naselenie) {
        this.name = name;
        this.capital = capital;
        this.territory = territory;
        this.naselenie = naselenie;
    }

    CountryBox() {
        this("неизвестно", "неизвестно", 0, 0);
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public double getTerritory() {
        return territory;
    }

    public int getNaselenie() {
        return naselenie;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public void setTerritory(double territory) {
        this.territory = territory;
    }

    public void setNaselenie(int naselenie) {
        this.naselenie = naselenie;
    }

    public double plotnostNas() {
        return naselenie / territory;
    }

    @Override
    public String toString() {
        return "CountryBox{" +
                "Название='" + name + '\'' +
                ", Столица='" + capital + '\'' +
                ", Площадь=" + territory +
                ", Население=" + naselenie +
                '}';
    }
}
