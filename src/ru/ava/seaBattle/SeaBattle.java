package ru.ava.seaBattle;

import java.util.Scanner;

/**
 * Класс показывающий возможности работы с объектом класса Ship
 *
 * @author Andreeva V.A
 */
public class SeaBattle {
    public static void main(String[] args) {

        int shipLength = randomShipLength();
        int sluice = randomSluice(shipLength);

        Ship ship = new Ship(randomStart(shipLength, sluice), shipLength);

        startGame(ship, sluice);
    }

    /**
     * Возвращает рандомный размер корабля
     *
     * @return размер корабля
     */
    private static int randomShipLength() {
        return 1 + (int) (Math.random() * 4);
    }

    /**
     * Возвращает рандомный размер шлюза
     *
     * @param shipLength длина корабля
     * @return размер шлюза
     */
    private static int randomSluice(int shipLength) {
        return (shipLength) + (int) (Math.random() * 20);
    }

    /**
     * Возвращает рандомную начальную координату корабля
     *
     * @param shipLength размер корабля
     * @param sluice размер шлюза
     * @return начальную координату корабля
     */
    private static int randomStart(int shipLength, int sluice) {
        int random;
        do {
            random = 1 + (int) (Math.random() * sluice);
        } while (random < 0 || random >= sluice - shipLength);
        return random;
    }

    /**
     * Выводит результаты игры пользователя
     *
     * @param ship объект корабль
     * @param sluice размер шлюза
     */
    private static void startGame(Ship ship, int sluice) {
        Scanner scanner = new Scanner(System.in);
        String message;
        int tries = 1;
        int shot;
        do {
            System.out.println("Введите координату выстрела от 0 до " + (sluice - 1) + ":");
            shot = scanner.nextInt();
            message = ship.shot(shot);
            System.out.println(message);
            if (message.equals("Попал") || message.equals("Мимо")) {
                tries++;
            }
        }
        while (!message.equals("Потоплен"));
        System.out.println("Вы потопили корабль с " + tries + " попытки(-ок)");
    }
}
 