package ru.ava.seaBattle;

import java.util.ArrayList;

@SuppressWarnings("unused")
class Ship {

    private ArrayList<Integer> locationOfShip;

    Ship(int start, int shipLength) {
        locationOfShip = new ArrayList<>();
        for (int i = start; i < start + shipLength; i++) {
            locationOfShip.add(i);
        }

    }

    /**
     * Выводит результат выстрелов игрока (всей игры)
     *
     * @param shot координата выстрела игрока
     */
    String shot(int shot) {
        String message = "Мимо";
        if (locationOfShip.contains(shot)) {
            message = "Попал";
            locationOfShip.remove((Integer) shot);
            if (locationOfShip.isEmpty()) {
                message = "Потоплен";
            }
        }
        return message;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "locationOfShip=" + locationOfShip +
                '}';
    }

}


