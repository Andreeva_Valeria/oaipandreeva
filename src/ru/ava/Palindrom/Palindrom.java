package ru.ava.Palindrom;

import java.util.Scanner;

/**
 * Класс определения палиндромности вводимой строки
 *
 * @author Andreeva V.A.
 */
public class Palindrom {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите ваше выражение или слово - ");
        String s = scanner.nextLine();
        s = s.replaceAll("[^A-Za-zА-Яа-я0-9]", "");
        if (s.toLowerCase().equals((new StringBuffer(s)).reverse().toString().toLowerCase())) {
            System.out.println("Палиндром! :)" );
        } else {
            System.out.println("Не палиндром! :(");
        }
    }
}
