package ru.ava.calculator2;

public class MathInt {
    public static int add(int firstNumber, int secondNumber) {
        return firstNumber + secondNumber;
    }

    public static int subtract(int firstNumber, int secondNumber) {
        return firstNumber - secondNumber;
    }

    public static double divide(double firstNumber, double secondNumber) {
        if (secondNumber == 0) {
            System.out.println("Вы не можете делить на ноль");
            return 0;
        }
        return firstNumber / secondNumber;
    }

    public static int multiply(int firstNumber, int secondNumber) {
        return firstNumber * secondNumber;
    }

    public static int remainder(int firstNumber, int secondNumber) {
        return firstNumber % secondNumber;
    }

    public static int power(double firstNumber, double secondNumber) {
        double result = firstNumber;
        if (secondNumber == 0) {
            return 1;
        }
        if (secondNumber > 0) {
            for (int index = 2; index <= secondNumber; index++) {
                result = result * firstNumber;
            }
        } else if (secondNumber < 0) {
            for (int index = -2; index <= secondNumber; index--) {
                result = result * firstNumber;
            }
            result = 1 / result;
        }
        return (int) result;

    }
}

