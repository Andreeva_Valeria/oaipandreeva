package ru.ava.calculator2;

import java.util.Scanner;

/**
 * Класс калькулятора
 *
 * @author Andreeva V.A.
 */
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String virazhenie;
        int firstNumber, secondNumber;

        System.out.println("Введите ваше выражение");
        System.out.println("\nValid оператор:");
        System.out.println("+сложение\n- вычитание\n* умножение\n/ делениу\n% остатокОтДеления\n^ степень\n");
        virazhenie = input.nextLine();

        String[] peremennye = virazhenie.split(" ");
        firstNumber = Integer.valueOf(peremennye[0]);
        secondNumber = Integer.valueOf(peremennye[1]);


        switch (peremennye[1]) {
            case "+":
                System.out.println("\n" + virazhenie + " = " + MathInt.add(firstNumber, secondNumber));
                break;
            case "-":
                System.out.println("\n" + virazhenie + " = " + MathInt.subtract(firstNumber, secondNumber));
                break;
            case "*":
                System.out.println("\n" + virazhenie + " = " + MathInt.multiply(firstNumber, secondNumber));
                break;
            case "/":
                System.out.println("\n" + virazhenie + " = " + MathInt.divide(firstNumber, secondNumber));
                break;
            case "%":
                System.out.println("\n" + virazhenie + " = " + MathInt.remainder(firstNumber, secondNumber));
                break;
            case "^":
                System.out.println("\n" + virazhenie + " = " + MathInt.power(firstNumber, secondNumber));
                break;
        }
    }
}

