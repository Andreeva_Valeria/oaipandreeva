package ru.ava.bank;

import java.util.Scanner;
import java.util.ArrayList;

/**
 * Класс, показывающий возможности работы с объектами класса BankAccount
 *
 * @author Andreeva V.A.
 */
public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        BankAccount bankAccount1 = new BankAccount(12354.76, 6);
        BankAccount bankAccount2 = new BankAccount(24156, 7);
        BankAccount bankAccount3 = new BankAccount(372654.56, 11);
        BankAccount bankAccount4 = new BankAccount(345.765, 5);
        BankAccount bankAccount5 = new BankAccount(25346, 10);
        ArrayList<BankAccount> bankAccounts = new ArrayList<>();
        bankAccounts.add(bankAccount1);
        bankAccounts.add(bankAccount2);
        bankAccounts.add(bankAccount3);
        bankAccounts.add(bankAccount4);
        bankAccounts.add(bankAccount5);
        bankAccounts.add(inputAccount());

        stateOfAccount(bankAccounts);
    }

    /**
     * Возвращает созданный объект класса BankAccount
     *
     * @return объект класса BankAccount
     */
    private static BankAccount inputAccount() {

        System.out.println("Введите баланс : ");
        double balance = scanner.nextDouble();
        System.out.println("Введите процентную ставку : ");
        double interestRate = scanner.nextDouble();

        return new BankAccount(balance, interestRate);
    }

    /**
     * Выводит состояние банковского счета с учетом процентных начислений за один год
     *
     * @param bankAccounts список аккаунтов
     */
    private static void stateOfAccount(ArrayList<BankAccount> bankAccounts) {
        double a;
        for (BankAccount bankAccount : bankAccounts) {
            a = bankAccount.getBalance() + bankAccount.getBalance() * (bankAccount.getInterestRate() / 100);
            System.out.println("Состояние счета " + bankAccount + " через год: " + a);
        }
    }
}
