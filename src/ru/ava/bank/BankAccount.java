package ru.ava.bank;

public class BankAccount {
    private double balance;
    private double interestRate;

    BankAccount(double balance, double interestRate) {
        this.balance = balance;
        this.interestRate = interestRate;
    }

    double getBalance() {
        return balance;
    }

    double getInterestRate() {
        return interestRate;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "Баланс = " + balance +
                ", Процентная ставка = " + interestRate +
                '}';
    }
}
