package ru.ava.Robot;

import java.util.Scanner;


public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {


        Robot robot = input();
        System.out.println(robot);
        robot.turnRight();
        System.out.println(robot);

        System.out.print("Введите конечное значение x:");
        int endX = scanner.nextInt();
        System.out.print("Введите конечное значение y:");
        int endY = scanner.nextInt();

        robot.move(endX, endY);
        System.out.println(robot);
    }


    private static Robot input() {
        Directoria directionNow = Directoria.UP;
        System.out.print("Введите начальное значение x:");
        int x = scanner.nextInt();
        System.out.print("Введите начальное значение y:");
        int y = scanner.nextInt();
        System.out.print("Введите изначальное направление взгляда /вверх,вниз,вправо,влево/:");
        scanner.nextLine();
        String lookAt = scanner.nextLine();

        switch (lookAt) {
            case "вверх":
                directionNow = Directoria.UP;
                break;
            case "вниз":
                directionNow = Directoria.DOWN;
                break;
            case "вправо":
                directionNow = Directoria.RIGHT;
                break;
            case "влево":
                directionNow = Directoria.LEFT;
                break;
            default:
                break;
        }
        return new Robot(x, y, directionNow);

    }
}




