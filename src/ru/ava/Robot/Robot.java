package ru.ava.Robot;

public class Robot {

    private int x;
    private int y;
    private Directoria direction;

    Robot(int x, int y, Directoria direction) {

        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    Robot() {
        this(0, 0, Directoria.UP);

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Directoria getDirection() {
        return direction;
    }


    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                ", y=" + y +
                ", direction='" + direction + '\'' +
                '}';
    }

    public void oneStep() {

        switch (direction) {
            case UP:
                y++;
                break;

            case RIGHT:
                x++;
                break;

            case DOWN:
                y--;
                break;

            case LEFT:
                x--;
                break;
            default:
                break;

        }
    }

    void turnRight() {
        switch (direction) {
            case UP:
                direction = Directoria.RIGHT;
                break;
            case RIGHT:
                direction = Directoria.DOWN;
                break;
            case DOWN:
                direction = Directoria.LEFT;
                break;
            case LEFT:
                direction = Directoria.UP;
                break;
            default:
                break;
        }
    }

    public void move(int endX, int endY) {
        if (endX > x) {
            while (direction != Directoria.RIGHT) {
                turnRight();
            }
            while (x != endX) {
                oneStep();
            }
        }
        if (endX < x) {
            while (direction != Directoria.LEFT) {
                turnRight();
            }
            while (x != endX) {
                oneStep();
            }
        }
        if (endY > y) {
            while (direction != Directoria.UP) {
                turnRight();
            }
            while (y != endY) {
                oneStep();
            }
        }
        if (endY < y) {
            while (direction != Directoria.DOWN) {
                turnRight();
            }
            while (y != endY) {
                oneStep();
            }
        }
    }
}

