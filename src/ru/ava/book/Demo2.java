package ru.ava.book;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс показывающий возможность работы с объектами класса Book
 *
 * @author Andreeva V.A
 */
public class Demo2 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Book book1 = new Book("Пушкин", "Собрание сочинений", 1820);
        Book book2 = new Book("Булгаков", "Мастер и маргарита", 1920);
        Book book3 = new Book("Бредбери", "Вино из одуванчиков", 1957);
        Book book4 = new Book("Полярный", "Сказка о самоубийстве", 2018);
        Book book5 = new Book("Пехов", "Ловушка для духа", 2013);

        ArrayList<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);
        books.add(input());
        printMessageBooks(books);
        printComparison(books);
    }

    private static Book input() {
        System.out.println("Введите автора : ");
        String surname = scanner.nextLine();
        System.out.println("Введите название книги : ");
        String name = scanner.nextLine();
        System.out.println("Введите год издания книги : ");
        int year = scanner.nextInt();
        return new Book(surname, name, year);
    }

    /**
     * Выводит результат сравнения года выпуска текущей и получаемой книги
     *
     * @param books массив книг
     */
    private static void printMessageBooks(ArrayList<Book> books) {
        int booksSize = books.size();
        if (books.get(0).isSameYears(books.get(booksSize - 1))) {
            System.out.println("Год издания книги - " + books.get(0).getNameOfBook() + " и книги " + books.get(booksSize - 1).getNameOfBook() + " - равны ");
        } else {
            System.out.println("Год издания книги - " + books.get(0).getNameOfBook() + " и книги " + books.get(booksSize - 1).getNameOfBook() + " - не равны");
        }
    }

    /**
     * Выводит название книги, если она была выпущена в 2018 году
     *
     * @param books массив книг
     */
    private static void printComparison(ArrayList<Book> books) {
        for (Book book : books) {
            if (book.getYearOfPublication() == 2018) {
                System.out.println("Книга " + book.getNameOfBook() + " - 2018-го года издания \n" + book);

            }
        }
    }
}