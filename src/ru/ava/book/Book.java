package ru.ava.book;
@SuppressWarnings("unused")
public class Book {
    private String surnameOfAuthor;
    private String nameOfBook;
    private int yearOfPublication;

    public Book(String surnameOfAuthor, String nameOfBook, int yearOfPublication) {
        this.surnameOfAuthor = surnameOfAuthor;
        this.nameOfBook = nameOfBook;
        this.yearOfPublication = yearOfPublication;
    }

    Book() {
        this("неизвестно", "неизвестно", 0);
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }

    public String getSurnameOfAuthor() {
        return surnameOfAuthor;
    }

    public String getNameOfBook() {
        return nameOfBook;
    }

    /**
     * Возвращает true если года издания двух книг равны, иначе false
     *
     * @param book книга
     * @return true если года издания двух книг равны, иначе false
     */
    public boolean isSameYears(Book book) {
        return this.getYearOfPublication() == this.yearOfPublication;
    }

    @Override
    public String toString() {
        return "Book{" +
                "surnameOfAuthor='" + surnameOfAuthor + '\'' +
                ", nameOfBook='" + nameOfBook + '\'' +
                ", yearOfPublication=" + yearOfPublication +
                '}';
    }
}

