package ru.ava.book;

/**
 * Класс показывающий возможность работы с объектами класса Book
 *
 * @author Andreeva V.A
 */
public class Demo {

    public static void main(String[] args) {

        Book book1 = new Book("Пушкин", "Собрание сочинений", 1820);
        Book book2 = new Book("Булгаков", "Мастер и маргарита", 1920);
        Book book3 = new Book("Бредбери", "Вино из одуванчиков", 1957);
        Book book4 = new Book("Полярный", "Сказка о самоубийстве", 2018);
        Book book5 = new Book("Пехов", "Ловушка для духа", 2013);

        Book[] books = {book1, book2, book3, book4, book5};
        years2018(books);
        oneYearOfBook(book1.isSameYears(book2));
    }

    /**
     * Выводит результат сравнения года выпуска текущей и получаемой книги
     *
     * @param sameYears одинаковые года публикации
     */
    private static void oneYearOfBook(boolean sameYears) {
        if (sameYears) System.out.println("Первая и вторая книги были выпущены в одном и том же году ");
        if (!sameYears) System.out.println("Первая и вторая книги не были выпущены в одном и том же году ");
    }

    /**
     * Выводит название книги, если она была выпущена в 2018 году
     *
     * @param books массив книг
     */
    private static void years2018(Book[] books) {
        boolean flag = true;
        for (Book book : books) {
            if (book.getYearOfPublication() == 2018) {
                System.out.println(book);
                flag = false;
            }
        }
        if (flag) {
            System.out.println("Книг 2018 года нет");
        }
    }
}

