package ru.ava.baggage;

/**
 * Класс представления характеристик багажа
 *
 * @author Andreeva V.A.
 */
public class Baggage {
    private String sername;
    private int bagplace;
    private double bagweight;

    public Baggage(String sername, int bagplace, double bagweight) {
        this.sername = sername;
        this.bagplace = bagplace;
        this.bagweight = bagweight;
    }

    public String getSername() {
        return sername;
    }

    public int getBagplace() {
        return bagplace;
    }

    public double getBagweight() {
        return bagweight;
    }

    /**
     * Позволяет определить относится багаж к ручной клади или нет
     *
     * @return булевое значение
     */
    public boolean handBag() {
        return (bagplace == 1 && bagweight <= 10);
    }
}
