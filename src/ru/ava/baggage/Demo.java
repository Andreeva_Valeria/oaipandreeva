package ru.ava.baggage;

/**
 * Класс показывающий возможности работы с объектами класса Baggage.
 *
 * @author Andreeva V.A.
 */

public class Demo {
    public static void main(String[] args) {
        Baggage one = new Baggage("Иванов", 3, 15);
        Baggage two = new Baggage("Петров", 1, 4.7);
        Baggage three = new Baggage("Сидоров", 5, 23);
        Baggage four = new Baggage("Васин", 1, 1);
        Baggage five = new Baggage("Федотов", 7, 10);

        Baggage[] baggages = {one, two, three, four, five};
        outArrayOfBaggage(baggages);
    }

    /**
     * Позволяет вывести на экран фамилии пассажиров с ручной кладью
     *
     * @param baggages
     */
    private static void outArrayOfBaggage(Baggage[] baggages) {
        for (Baggage baggage : baggages) {
            if (baggage.handBag()) {
                System.out.println(baggage.getSername());
            }
        }
    }
}
