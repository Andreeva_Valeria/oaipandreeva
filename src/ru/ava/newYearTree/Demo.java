package ru.ava.newYearTree;

import java.util.Scanner;

/**
 * Класс, показывающий возможности работы с объектами класса NewYearTree
 *
 * @author Andreeva V.A.
 */
public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String wish;
        do {
            System.out.print("За какую сумму вы готовы купить елку? - ");
            double cost = scanner.nextDouble();
            scanner.nextLine();
            System.out.print("Какую Вы хотите живую елку?(да/нет) - ");
            String type = scanner.nextLine();

            NewYearTree newYearTree = new NewYearTree(type);
            double height = (double) newYearTree.heightOfTree(newYearTree, cost);

            System.out.print(" Мы можем предложить вам елку высотой " + String.format("%.2f ", height) + " м. ");
            System.out.print(" Вы довольны?(да/нет) - ");
            wish = scanner.nextLine();

        } while (wish.equalsIgnoreCase("нет"));

        System.out.println("Спасибо за покупку. Приходите к нам снова!");
    }
}

