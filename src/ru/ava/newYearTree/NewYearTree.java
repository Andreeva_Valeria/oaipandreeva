package ru.ava.newYearTree;

public class NewYearTree {
    private double height;
    private String type;

    NewYearTree(String type) {
        this.height = 1;
        this.type = type;
    }

    private String getType() {
        return type;
    }

    /**
     * Возвращает высоту елки по указанными клиентом стоимости и типу
     *
     * @param newYearTree елка
     * @param cost стоимость
     *
     * @return высоту елки
     */
    Object heightOfTree(NewYearTree newYearTree, double cost) {
        double height = 0;
        if (newYearTree.getType().equalsIgnoreCase("Нет")) {
            height = cost / 700;
        }
        if (newYearTree.getType().equalsIgnoreCase("Да")) {
            height = cost / 500;
        }
        return height;
    }

    @Override
    public String toString() {
        return "Новогодняя ёлка {" +
                " размер - " + height + '\'' +
                ", материал - " + type + '\'' +
                '}';
    }
}

