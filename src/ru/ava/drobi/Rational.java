package ru.ava.drobi;

@SuppressWarnings("unused")
public class Rational {
    private int numerator;
    private int denominator;

    Rational(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    int getNumerator() {
        return numerator;
    }

    int getDenominator() {
        return denominator;
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }

    /**
     * Возвращает результат умножения двух дробей
     *
     * @param x дробь
     * @return произведение двух дробей
     */
    Rational multiply(Rational x) {
        Rational rational3;
        int num = numerator * x.getNumerator();
        int den = denominator * x.getDenominator();
        rational3 = new Rational(num, den).simplify();
        return rational3;
    }

    /**
     * Возвращает результат деления двух дробей
     *
     * @param x дробь
     * @return частное двух дробей
     */
    Rational divide(Rational x) {
        Rational rational4;
        int num = numerator * x.getDenominator();
        int den = denominator * x.getNumerator();
        rational4 = new Rational(num, den).simplify();
        return rational4;
    }

    /**
     * Возвращает результат сложения двух дробей
     *
     * @param x дробь
     * @return сумму двух дробей
     */
    Rational add(Rational x) {
        Rational rational5;
        if (denominator == x.denominator) {
            rational5 = new Rational(x.numerator + numerator, denominator);
        } else {
            int num1 = numerator * x.getDenominator();
            int num2 = denominator * x.getNumerator();
            int num = num1 + num2;
            int den = denominator * x.getDenominator();
            rational5 = new Rational(num, den).simplify();
        }
        return rational5;
    }

    /**
     * Возвращает результат вычитания двух дробей
     *
     * @param x дробь
     * @return разность двух дробей
     */
    Rational subtract(Rational x) {
        int newNum = ((numerator * x.denominator) -
                (x.numerator * denominator)
        );
        int newDen = denominator * x.denominator;

        return new Rational(newNum, newDen).simplify();
    }

    /**
     * Возвращает результат сокращения дроби
     *
     * @return сокращенную дробь
     */
    Rational simplify() {
        int a = numerator;
        int b = denominator;

        a /= nod(a, b);
        b /= nod(a, b);

        return new Rational(a, b);
    }

    /**
     * Возвращает наибольший общий делитель числителя и знаменателя дроби
     *
     * @param a числитель
     * @param b знаменатель
     *
     * @return НОД
     */
    static int nod(int a, int b) {
        a = Math.abs(a);
        b = Math.abs(b);
        while (a != b) {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }
}


