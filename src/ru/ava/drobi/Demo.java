package ru.ava.drobi;

import java.util.Scanner;

/**
 * Класс,показывающий возможности работы с объектами класса Rational
 *
 * @author Andreeva V.A.
 */
public class Demo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите выражение вида a/b знак операции a/b");
        String expression = scanner.nextLine();

        String[] withoutSpace = expression.replaceAll("[\\s +:*-]", "/").split("/");

        int a = Integer.parseInt(withoutSpace[0]);
        int b = Integer.parseInt(withoutSpace[1]);
        Rational firstRational = new Rational(a, b).simplify();

        int c = Integer.parseInt(withoutSpace[4]);
        int d = Integer.parseInt(withoutSpace[5]);
        Rational secondRational = new Rational(c, d).simplify();

        operation(expression, firstRational, secondRational);
    }

    private static void operation(String expression, Rational firstRational, Rational secondRational) {

        String[] withoutSpace = expression.split("\\s");
        String operation = withoutSpace[1];
        Rational rational3;

        switch (operation) {
            case "+":
                rational3 = firstRational.add(secondRational);
                System.out.println(rational3.getNumerator() + "/" + rational3.getDenominator());
                break;
            case "-":
                rational3 = firstRational.subtract(secondRational);
                System.out.println(rational3.getNumerator() + "/" + rational3.getDenominator());
                break;
            case "*":
                rational3 = firstRational.multiply(secondRational);
                System.out.println(rational3.getNumerator() + "/" + rational3.getDenominator());
                break;
            case ":":
                rational3 = firstRational.divide(secondRational);
                System.out.println(rational3.getNumerator() + "/" + rational3.getDenominator());
                break;
            default:
                System.out.println("Неправильный знак операций");
                break;
        }
    }
}