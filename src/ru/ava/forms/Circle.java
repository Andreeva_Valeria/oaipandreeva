package ru.ava.forms;

public class Circle extends Shape {
    /**
     * Центр круга - точка с координатами x и y
     */
    private Point center;
    /**
     * Радиус круга
     */
    private double radius;


    /**
     * Инициализирует вновь созданный объект в соответствии с
     *
     * @param color  цвет
     * @param center координата центра круга
     * @param radius радиус
     */
    Circle(Color color, Point center, double radius) {
        super(color);
        this.center = center;
        this.radius = radius;
    }

    /**
     * Возвращает площадь круга
     *
     * @return площадь круга
     */
    @Override
    public double area() {
        return Math.PI * radius * radius;
    }

    /**
     * Возвращает строковое представление объекта
     *
     * @return строка, содержащая цвет, центр и радиус круга
     */
    @Override
    public String toString() {
        return "Circle{" +
                "Color=" + getColor() +
                "center=" + center +
                ", radius=" + radius +
                '}';
    }
}
