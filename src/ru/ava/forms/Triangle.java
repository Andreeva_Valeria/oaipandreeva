package ru.ava.forms;

/**
 * Треугольник со сторонами a, b, c.
 */
public class Triangle extends Shape {
    private Point a;
    private Point b;
    private Point c;

    /**
     * Инициализирует созданный объект
     *
     * @param color цвет
     * @param a     сторона а
     * @param b     сторона b
     * @param c     сторона c
     */
    Triangle(Color color, Point a, Point b, Point c) {
        super(color);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * Возвращает площадь треугольника
     *
     * @return площадь треугольника
     */
    @Override
    public double area() {
        return 1 / 2 * (b.getX() - a.getX()) * (c.getY() - a.getY()) - (c.getX() - a.getX()) * (b.getY() - a.getY());
    }

    /**
     * Возвращает строковое представление объекта
     *
     * @return строка, содержащая цвет, центр и радиус круга
     */
    @Override
    public String toString() {
        return "Triangle{" +
                "color=" + getColor() +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}
