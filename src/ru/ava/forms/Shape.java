package ru.ava.forms;

/**
 * Абстрактный класс для представлкния цветных фигур
 *
 * @author AVA
 */

public abstract class Shape {
    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public abstract double area();
}
