package ru.ava.forms;

import java.util.Arrays;

/**
 * Класс создания и хранения фигур с характеристиками
 */
public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(Color.BLACK, new Point(), 5);
        Triangle triangle = new Triangle(Color.BLUE, new Point(0, 0),
                new Point(1, 0), new Point(0, 1));
        Square square = new Square(Color.RED, 5, new Point(1, 1));

        Shape[] shapes = {circle, triangle, square};
        printArrayElements(shapes);


        double maxShape = maxShapeArea(shapes);
        System.out.println("Фигура с максимальной площадью:" + maxShape);

    }

    /**
     * Класс вывода массива фигур с характеристиками
     *
     * @param shapes массив фигур
     */
    private static void printArrayElements(Shape[] shapes) {
        System.out.println(Arrays.toString(shapes));

    }

    /**
     * Расчет максимальной площади фигуры
     *
     * @param shapes массив с характеристиками фигур
     * @return максимальное значение
     */
    private static double maxShapeArea(Shape[] shapes) {
        double max = 0;
        for (int i = 0; i < shapes.length; i++) {
            double density = shapes[i].area();
            if (max < density) {
                max = density;
            }
        }
        return max;
    }

}


