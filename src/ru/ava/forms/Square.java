package ru.ava.forms;


/**
 * Сторона и угол квадрата
 */
public class Square extends Shape {
    private double side;
    private Point corner;

    /**
     * Инициализирует созданный обьект
     *
     * @param color  цвет
     * @param side   сторона
     * @param corner угол
     */
    Square(Color color, double side, Point corner) {
        super(color);
        this.side = side;
        this.corner = corner;
    }

    /**
     * Возвращает площадь квадрата
     *
     * @return площадь квадрата
     */
    @Override
    public double area() {
        return side * side;
    }

    /**
     * Возвращает строковое представление объекта
     *
     * @return строка, содержащая цвет, центр и радиус круга
     */
    @Override
    public String toString() {
        return "Square{" +
                "color=" + getColor() +
                "side=" + side +
                ", corner=" + corner +
                '}';
    }
}