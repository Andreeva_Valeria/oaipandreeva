package ru.ava.forms;

/**
 * Класс для представления точки в двуерном пространстве
 *
 * @author AVA
 */

public class Point {
    private double x;
    private double y;


    Point(double x, double y){
        this.x = x;
        this.y = y;
    }
    Point() {
        this(0,0);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
