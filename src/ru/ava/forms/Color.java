package ru.ava.forms;

/**
 * Содержит возможные цвета фигур
 *
 * @author Andreeva V.A.
 */
public enum Color {
    WHITE,
    BLACK,
    RED,
    GREEN,
    BLUE,
    YELLOW,
}
