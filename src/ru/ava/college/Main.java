package ru.ava.college;

import java.util.Scanner;
import java.util.ArrayList;

/**
 * Класс, пказывающий возможности работы с объктами родительского класса Person
 *
 * @author Andreeva.V.A
 */
public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        Student student1 = new Student("Рылов", Gender.MALE, 2017, "09.02.07");
        Student student2 = new Student("Анисимова", Gender.FEMALE, 2017, "09.02.07");
        Student student3 = new Student("Крылова", Gender.FEMALE, 2018, "09.02.07");
        Student student4 = new Student("Сидоров", Gender.MALE, 2017, "09.02.07");
        Student student5 = new Student("Федорова", Gender.FEMALE, 2017, "09.02.07");
        Student student6 = new Student("Котов", Gender.MALE, 2019, "09.02.07");
        Student student7 = new Student("Котова", Gender.FEMALE, 2017, "09.02.07");

        Teacher teacher1 = new Teacher("Гальцова", Gender.FEMALE, "База данных", true);
        Teacher teacher2 = new Teacher("Баранов", Gender.MALE, "Физкультура", false);
        Teacher teacher3 = new Teacher("Латышенко", Gender.FEMALE, "Ин.яз.", true);
        Teacher teacher4 = new Teacher("Зубов", Gender.MALE, "Физика", true);
        Teacher teacher5 = new Teacher("Рубцова", Gender.FEMALE, "Математика", false);

        ArrayList<Student> students = new ArrayList<>();
        students.add(student1);
        students.add(student2);
        students.add(student3);
        students.add(student4);
        students.add(student5);
        students.add(student6);
        students.add(student7);
        students.add(inputStudents());

        ArrayList<Teacher> teachers = new ArrayList<>();
        teachers.add(teacher1);
        teachers.add(teacher2);
        teachers.add(teacher3);
        teachers.add(teacher4);
        teachers.add(teacher5);
        teachers.add(inputTeachers());

        ArrayList<Person> personArrayList = new ArrayList<>();
        personArrayList.add(teacher1);
        personArrayList.add(teacher2);
        personArrayList.add(teacher3);
        personArrayList.add(teacher4);
        personArrayList.add(teacher5);
        personArrayList.add(student1);
        personArrayList.add(student2);
        personArrayList.add(student3);
        personArrayList.add(student4);
        personArrayList.add(student5);
        personArrayList.add(student6);
        personArrayList.add(student7);

        int girl2017 = girl2017(students);
        System.out.println("Количество девушек, поступивших в 2017 году - " + girl2017);
        System.out.println("Сведения о преподавателях, являющихся кураторами: ");
        teachersCurator(teachers);
        System.out.println("Сведения о студентах и преподавателях мужского пола: ");
        maleGender(personArrayList);
    }

    /**
     * Выводит сведения о студентах и преподавателях мужского пола
     *
     * @param personArrayList студенты и преподаватели
     */
    private static void maleGender(ArrayList<Person> personArrayList) {
        for (Person person : personArrayList) {
            if (person.getGender() == Gender.MALE) {
                System.out.println(person);
            }
        }
    }

    /**
     * Выводит сведения о преподавателях, являющихся кураторами
     *
     * @param teachers  преподаватели
     */
    private static void teachersCurator(ArrayList<Teacher> teachers) {
        for (Teacher teacher : teachers) {
            if (teacher.isCurator()) {
                System.out.println(teacher);
            }
        }
    }

    /**
     * Возвращает кол-во девушек, поступивших в колледж в 2017 году
     *
     * @param students студенты
     *
     * @return кол-во девушек, поступивших в колледж в 2017 году
     */
    private static int girl2017(ArrayList<Student> students) {
        int x = 0;
        for (Student student : students) {
            if (student.getGender() == Gender.FEMALE && student.getYearOfAdmission() == 2017) {
                x++;
            }
        }
        return x;
    }

    /**
     * Возвращает созданный объект класса Student
     *
     * @return объект класса Student
     */
    private static Student inputStudents() {
        System.out.println("Введите Фамилию студента : ");
        String surname = scanner.nextLine();
        System.out.println("Введите пол м or ж : ");
        String gender = scanner.nextLine();

        Gender gender1;
        if (gender.equalsIgnoreCase("м")) {
            gender1 = Gender.MALE;
        } else {
            gender1 = Gender.FEMALE;
        }

        System.out.println("Введите год поступления : ");
        int yearOfAdmission = scanner.nextInt();
        System.out.println("Введите код специальности : ");
        scanner.nextLine();
        String code = scanner.nextLine();

        return new Student(surname, gender1, yearOfAdmission, code);
    }

    /**
     * Возвращает созданный объект класса Teachers
     *
     * @return объект класса Teachers
     */
    private static Teacher inputTeachers() {
        System.out.println("Введите Фамилию преподавателя : ");
        String surname = scanner.nextLine();
        System.out.println("Введите пол (м/ж) : ");
        String gender = scanner.nextLine();

        Gender gender1;
        if (gender.equalsIgnoreCase("м")) {
            gender1 = Gender.MALE;
        } else {
            gender1 = Gender.FEMALE;
        }

        System.out.println("Введите дисциплину : ");
        String discipline = scanner.nextLine();
        System.out.println("Является ли куратором (да/нет)? : ");
        String curator = scanner.nextLine();

        boolean curator1;
        curator1 = curator.equalsIgnoreCase("да");

        return new Teacher(surname, gender1, discipline, curator1);
    }
}
