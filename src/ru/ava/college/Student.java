package ru.ava.college;

public class Student extends Person {
    private int yearOfAdmission;
    private String code;

    Student(String surname, Gender gender, int yearOfAdmission, String code) {
        super(surname, gender);
        this.yearOfAdmission = yearOfAdmission;
        this.code = code;
    }

    int getYearOfAdmission() {
        return yearOfAdmission;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Студент {" +
                "Год поступления =" + yearOfAdmission +
                ", Код специальности ='" + code + '\'' +
                '}';
    }
}
