package ru.ava.college;

@SuppressWarnings("unused")
public class Teacher extends Person {
    private String discipline;
    private boolean curator;

    Teacher(String surname, Gender gender, String discipline, boolean curator) {
        super(surname, gender);
        this.discipline = discipline;
        this.curator = curator;
    }

    public String getDiscipline() {
        return discipline;
    }

    boolean isCurator() {
        boolean flag = false;
        if (curator) {
            flag = true;
        }
        return flag;
    }

    @Override
    public String toString() {
        return "Преподаватель{" +
                "дисциплина='" + discipline + '\'' +
                ", является ли куратором=" + curator +
                '}';
    }
}
