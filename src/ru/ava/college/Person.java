package ru.ava.college;

@SuppressWarnings("unused")
public class Person {
    private String surname;
    private Gender gender;

    Person(String surname, Gender gender) {
        this.surname = surname;
        this.gender = gender;
    }

    public String getSurname() {
        return surname;
    }

    Gender getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return "Person{" +
                "Фамилия ='" + surname + '\'' +
                ", Пол =" + gender +
                '}';
    }
}
