package ru.ava.songs;

import java.util.Scanner;

/**
 * Класс, показывающий возможности работы с объектами класса Song
 *
 * @author Andreeva V.A.
 */
public class Demo {

    public static void main(String[] args) {
        Song songOne = new Song("Пустота", "pyrokinesis", 219);
        Song songTwo = new Song("Rumors", "Neffex", 247);
        Song songThree = new Song("Invoker", "Reddogy", 119);
        Song songFour = new Song("RestInPeace", "BONES", 300);
        Song songFive = new Song("Fear", "Score", 275);
        Song[] song = {songOne, songTwo, songThree, songFour, songFive};
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите продолжительность песни в секундах : ");
        int a = scanner.nextInt();
        songDuration(song, a);
        categoryShort(song);
        oneCategory(songOne.isSameCategory(songFive));
    }

    /**
     * Выводит сведения о песнях с заданной продолжительностью
     *
     * @param songs массив песен
     */
    private static void songDuration(Song[] songs, int a) {
        boolean flag = true;
        for (Song song : songs) {
            if (a == song.getDuration()) {
                System.out.println("Песни с заданной продолжительностью: " + song.toString());
                flag = false;
            }
        }
        if (flag) {
            System.out.println("Песни с такой продолжительностью нет ");
        }
    }

    /**
     * Выводит сведения о песнях категории short
     *
     * @param songs массив песен
     */
    private static void categoryShort(Song[] songs) {
        for (Song song : songs) {
            String firstCategory = song.category();
            if (firstCategory.equals("short")) {
                System.out.println("Песни категории short: " + song.toString());
            }
        }
    }

    /**
     * Проверяет относятся ли к одной категории первая и последняя песня
     *
     * @param sameCategory сравнение категории текущего объекта с полученным
     */
    private static void oneCategory(boolean sameCategory) {
        if (sameCategory) System.out.println("Первая и последняя песня относятся к одной категории ");
        if (!sameCategory) System.out.println("Первая и последняя песня не относятся к одной категории ");
    }
}

